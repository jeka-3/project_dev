jQuery(document).ready(function ($) {

    // слайдер на главной
    $('.main__slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        autoplay: true,
        autoplaySpeed: 2000
    });  

    // + - товар
    $(document).ready(function () {
        $('.calc__minus').click(function () {
            var $input = $(this).parent().find('.calc__total');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.calc__plus').click(function () {
            var $input = $(this).parent().find('.calc__total');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });

    // mobile menu
    $('.header-mobile').on('click', function (e) {
        e.preventDefault();
        $('.mobile-nav').addClass('mobile-active');
    });
    $('.filter__modal-links-btn').on('click', function (e) {
        e.preventDefault();
        $('.filter__modal-links').addClass('filter__active');
    });
    $('.filter__modal-sort-btn').on('click', function (e) {
        e.preventDefault();
        $('.filter__modal-sort').addClass('filter__active');
    });


    $('.close-btn').on('click', function (e) {
        e.preventDefault();
        $('.mobile-nav').removeClass('mobile-active');
        $('.filter__modal-links').removeClass('filter__active');
        $('.filter__modal-sort').removeClass('filter__active');
    });  

    
    
    // мобильный футер
    $('.footer__catalog').on('click', function() {
        $(this).toggleClass('footer__head-active')
        $('.footer__content-nav-catalog').toggleClass('footer__nav-active');
    });
    $('.footer__catigories').on('click', function () {
        $(this).toggleClass('footer__head-active')
        $('.footer__content-nav-catigories').toggleClass('footer__nav-active');
    });

});
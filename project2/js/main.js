jQuery(document).ready(function ($) {  


  // мобильное меню
  $('.header-mobile').on('click', function (e) {
    e.preventDefault();
    $('.header__nav').addClass('mobile-active');
  });
  $('.header__nav-close').on('click', function (e) {
    e.preventDefault();
    $('.header__nav').removeClass('mobile-active');
  });

  // варианты собак
  $('.nav__block-menu').on('click', function (e) {
    e.preventDefault();
    $('.catalog__sidebar-nav-wrap').addClass('sidebar-active');
  });
  $('.sidebar-close').on('click', function (e) {
    e.preventDefault();
    $('.catalog__sidebar-nav-wrap').removeClass('sidebar-active');
  });

  // показать фильтр
  $('.filter__button').on('click', function (e) {
    e.preventDefault();
    $('.catalog__filter-wrap').addClass('filter-active');
  });
  $('.filter-close').on('click', function (e) {
    e.preventDefault();
    $('.catalog__filter-wrap').removeClass('filter-active');
  });

  $(".header").sticky({ topSpacing: 0 });  

  var btn = $('.arrow__top');
  $(window).scroll(function () {
    if ($(window).scrollTop() > 600) {
      btn.addClass('top__show');
    } else {
      btn.removeClass('top__show');
    }
  });
  btn.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
  });

  $('[data-fancybox]').fancybox();

  $('.product__card-slider-for').slick({
    fade: false,
    dots: false,
    lazyLoad: 'progressive',
    arrows: false,
    slidesToScroll: 1,
    slidesToShow: 1,
    infinite: true,
    swipe: true,
    asNavFor: '.product__card-slider-nav',
    responsive: [
      {
        breakpoint: 576,
        settings: {
          dots: true
        }
      }
    ]
  });
  $('.product__card-slider-nav').slick({
    dots: false,
    centerMode: true,
    centerPadding: '110px',
    focusOnSelect: true,
    vertical: true,
    infinite: true,
    arrows: false,
    slidesToShow: 2,
    slidesToScroll: 1,
    verticalSwiping: true,
    asNavFor: '.product__card-slider-for'
  });
});